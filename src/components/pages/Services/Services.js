import React, {useEffect,useState} from "react";
import { Footer } from "../../Footer/Footer";
import "./Services.css";
import Aboutimg from "../../../components/images/services.jpeg";
import Aos from "aos";
import  ClipLoader from "react-spinners/ClipLoader";


export const Services = () => {
  const [loading,setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    },1000)
  }, [])
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 600,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, {});
  return (
    <>
     { loading ? 
    <div className="loader" >
      <ClipLoader
      size={30}
      color={"#fff"}
      loading={loading}
      height={200} width={200}
      /></div>
      :
    <div className="Services_page">
      <div className="Services_content">
        <div className="Services--banner">
        <img src={Aboutimg} />
        <h1>Services</h1>
        </div>
        
        <div className="Services">
          <div className="Services_content--h1">
            <h1>COLOR GRADING </h1>
            <p data-aos="fade-up">
              We made our name as pioneers of colour, delivering invisible
              artistry<br/> for award-winning projects.<br/> Right now, Monet is moving
              grading forward remotely, bringing the highest quality, easy setup
              to any<br/> laptop, iOS device, monitor, TV or edit suite.
              
            </p>
            <h1>FINISHING</h1>
            <p data-aos="fade-up">
              We look after the final stages of online and finishing, to help<br/>
              you land exceptional images.<br/> With two dedicated online/Flame
              suites and a roster of the best talent, we deliver a wide range of<br />
              services tailored to your requirements – in-house or remotely.
            </p>
            <h1>VFX</h1>
            <p data-aos="fade-up">
              We offer our clients the ultimate in VFX, in partnership with most
              creative talents.<br/> If it’s not possible to shoot it, together we
              can make it.<br /> The teams we work with are highly experienced Senior
              VFX Artists known for delivering best quality videos.
            </p>
            <h1>FILM EMULATION</h1>
            <p data-aos="fade-up">
              We love the film aesthetic – it defined our vocation. Long
              fascinated with emulating film looks, we know how to<br/> elicit the
              feelings evoked by film’s unmistakable organic aesthetic.<br/>
              Extensive research and development and MONET proprietary tools
              mean we can tailor a unique look for any<br /> project. We also offer
              custom film<br/> profiling and LUT creation, allowing us to shape the
              look development.
            </p>
          </div>
        </div>
        <div>
          <Footer />
        </div>
      </div>
    </div>
}
    </>
  );
};
