import React,{useEffect,useState} from "react";
import "./About.css";
import Aboutimg from "../../../components/images/about.jpeg";

import { Footer } from "../../Footer/Footer";
import Aos from "aos";
import "aos/dist/aos.css";
import  ClipLoader from "react-spinners/ClipLoader";

export const About = () => {
  const [loading,setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    },1000)
  }, [])
  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 600,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  return (
    <>
     { loading ? 
    <div className="loader" >
      <ClipLoader
      size={30}
      color={"#fff"}
      loading={loading}
      height={200} width={200}
      /></div>
      :
    <div className="About_page">
      <div className="About_content">
        <div className="About_content--banner">
          <img src={Aboutimg} />
          <h1>About</h1>
        </div>

        <div className="About_content--p">
          <div  className="About_content--text">
            <h1>Monet post production company based on Kosovo</h1>
            <p data-aos="fade-up" className="About_content--text">
              MONET is a comprehensive post production facility based in the
              youngest country in the world Kosova, for film, documentaries,
              commercials and music videos.<br/> Known for its creative team,
              pioneering technology and global reach, MONET is trusted to
              deliver<br/> a quality product every time. It is home to one of the
              largest and most talented pools of editors, colorists<br /> and visual
              effects artists working in post production today with a client
              list that includes many of the<br/> most respected and talented film,
              music and entertainment professionals in the industry.
            </p><br/>
            <p data-aos="fade-up" >
              MONET offers solutions-based services ranging from editing, color
              correction and vfx through its virtual outposts,<br/> MONET is
              available to clients around the world.
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </div>
}
    </>
  );
};
