import React , { useRef,useState,useEffect}from "react";
import "./Contact.css";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import emailjs from "emailjs-com";
import  ClipLoader from "react-spinners/ClipLoader";


export const Contact = () => {
  const [loading,setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    },2000)
  }, [])
  function sendEmail(e) {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_ez2yakz",
        "template_pgs1eti",
        e.target,
        "user_P9mOWiqicotrwvVIFB7B1"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    e.target.reset();
  }
  return (
    <>
     { loading ? 
    <div className="loader" >
      <ClipLoader
      size={30}
      color={"#fff"}
      loading={loading}
      height={200} width={200}
      /></div>
      :
    <div className="Contact_page"  >
      <div className="Contact_content">
      <Container className="content">
          <Row className="div_contact--info">
          <Col className="contact_info" lg={4} md={12} sm={12}>
              <p >Email : contact@monet.tv</p>
              <p>Adress : Veternik,Prishtine[Kosovo] </p>
              <p>Number : 044 99 44 99</p>
            </Col>
            <Col className="input-monet" sm={12} lg={8} md={12}>
              <Form  onSubmit={sendEmail}>
                <Form.Group className="mb-3">
                  <Form.Control className="imput_width" name="name" type="text" placeholder="Name and Surname" />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Control
                    type="email"
                    name="user_email"
                    placeholder="Email"
                    className="imput_width"
                  />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Control
                    type="text"
                    name="subject"
                    placeholder="Subject"
                    className="imput_width"
                  />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Control
                    placeholder="Message"
                    name="message"
                    as="textarea"
                    rows={6}
                    className="message"
                  />
                </Form.Group>
                <Col>
                  <Button
                    variant="primary"
                    type="submit"
                    value="Send Message"
                    style={{ width: "70%" ,fontFamily: "Beckman"}}
                    className="button-send"
                  >
                    Send
                  </Button>
                </Col>
              </Form>
            </Col>
           
          </Row>
        </Container>
      </div>
    </div>
}
    </>
  );
};
