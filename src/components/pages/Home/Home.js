import React, { useState, useEffect } from "react";
import "./Home.css";
import { Footer } from "../../Footer/Footer";
import ClipLoader from "react-spinners/ClipLoader";

export const Home = () => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  return (
    < >
      {loading ? 
        <div className="loader">
          <ClipLoader
            size={30}
            color={"#fff"}
            loading={loading}
            height={200}
            width={200}
          />
        </div>
      : 
        <div className="Home_page">
          <div className="HeroSecttion">
            <div className="player">
              <video
               autoPlay
                muted 
                config={{ file: { attributes: { controlsList: 'nodownload' } } }}
                onContextMenu={e => e.preventDefault()}
                src="/images/MONET_Showreel.mp4" />
            </div>
          </div>
          <div className="home_footer">
            <Footer className="home_footer" />
          </div>
        </div>
      }
    </>
  );
};
