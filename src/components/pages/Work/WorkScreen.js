import React, { useState, useEffect } from "react";
import "./Work.css";
import { Footer } from "../../Footer/Footer";
import { Link } from "react-router-dom";
import { Row, Col, CardImg } from "react-bootstrap";
import works from "../../../Works";
import ReactPlayer from "react-player";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ClipLoader from "react-spinners/ClipLoader";

const WorkScreen = ({ match }) => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
      partialVisibilityGutter: 40, // this is needed to tell the amount of px that should be visible.
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      partialVisibilityGutter: 30, // this is needed to tell the amount of px that should be visible.
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      partialVisibilityGutter: 30, // this is needed to tell the amount of px that should be visible.
    },
  };
  const workscreen = works.find((w) => w._id === match.params.id);


  function nonMutatingSort(arr) {
    let randomWorks = [...arr];
      randomWorks.sort(() => Math.random() - 0.5)
      return randomWorks;
    }
 
  const filterRow = works.filter(worke => worke.name === worke.imagethub);

  return (
    <>
      {loading ? 
        <div className="loader">
          <ClipLoader
            size={30}
            color={"#fff"}
            loading={loading}
            height={200}
            width={200}
          />
        </div>
       : 
        <div className="Works_page">
          <div className="Works_page--contnet">
            <div className="row_works">
              <ReactPlayer
                poster={workscreen.imagethub}
                className="react-player"
                controls
                url={workscreen.url}
              />
            </div>
            <div>
              <div className="works_conent--p">
                <div className="works_conent--left">
                  <p>Project:</p>
                  <p>
                    {workscreen.name} - {workscreen.namesong}
                  </p>
                </div>
                <div className="works_conent--right">
                  <p>Post Production : {workscreen.production}</p>
                </div>
              </div>
            </div>
            <div className="hero_frames">
              <p>Hero Frames :</p>

              <Row>
                {works[workscreen._id - 1].imageheros.map((item) => (
                  <Col className="workscreen_img" xs={12} lg={4} md={6}>
                    <CardImg src={item.image} />
                  </Col>
                ))}
              </Row>
            </div>
            <div>
              <Row className="row_work">
                <p>Recommended Videos :</p>
                <Carousel partialVisible={true} responsive={responsive}>
                  {nonMutatingSort(works).map((work) => (
                    <Link to={`/workscreen/${work._id}`} >
                      
                    <CardImg  src={work.imagethub} />
                  </Link>
                  ))}
                </Carousel>
              </Row>
            </div>
          </div>
          <Footer />
        </div>
      }
    </>
  );
};

export default WorkScreen;
