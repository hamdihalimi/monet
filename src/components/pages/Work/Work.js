import React, { useState, useEffect } from "react";
import { CardImg, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Footer } from "../../Footer/Footer";
import "./Work.css";
import works from "../../../Works";
import Aos from "aos";
import "aos/dist/aos.css";
import ClipLoader from "react-spinners/ClipLoader";

export const Work = () => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  useEffect(() => {
    Aos.init({
      offset: 200,
      duration: 600,
      easing: "ease-in-sine",
      delay: 100,
    });
  }, []);
  return (
    <>
      {loading ? (
        <div className="loader">
          <ClipLoader
            size={30}
            color={"#fff"}
            loading={loading}
            height={200}
            width={200}
          />
        </div>
      ) : (
        <div className="Work_page">
          <div className="Work_page--content">
            <div className="card_workpage">
              <Row className="row_work">
                {works.map((work) => (
                  <Col className="card_thub" xs={12} lg={4} md={6}>
                    <Link to={`/workscreen/${work._id}`} >
                      <CardImg data-aos="fade-up" src={work.imagethub} />
                      <div class="overlay">
                        <span>{work.name}</span>
                        <p>{work.namesong}</p>
                      </div>
                    </Link>
                  </Col>
                ))}
              </Row>
            </div>
          </div>
          <Footer />
        </div>
      )}
    </>
  );
};
