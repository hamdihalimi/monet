import React from 'react';
import "./Footer.css"
import { Link } from 'react-router-dom';
import { BsFacebook, BsInstagram ,BsYoutube , BsTwitter ,BsLinkedin} from "react-icons/bs";


export const Footer = () => {
    return (
        <div className='footer-container'>
      <div class='footer-links'>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>About Us</h2>
            <Link to='/about'>About</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Contact Us</h2>
            <Link to='/contact'>Contact</Link>
            <Link to='/contact'>Support</Link>
          </div>
        </div>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>Videos</h2>
            <Link to='/videos'>Submit Video</Link>
            <Link to='/videos'>Agency</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Social Media</h2>
            <a href='https://www.instagram.com/colorbymonet/' target="_blank">Instagram</a>
            <a href="#" target="_blank">Facebook</a>
            <a href="https://www.youtube.com/c/ONIMA" target="_blank">Youtube</a>
            <a href="#" target="_blank">Twitter</a>
          </div>
        </div>
      </div>
      <section class='social-media'>
        <div class='social-media-wrap'>
         
          <p class='website-rights'>Monet © 2022</p>
          <div class='social-icons'>
          <a href="#" target="_blank">
              <BsFacebook />
            </a>
            <a href="https://www.instagram.com/colorbymonet/" target="_blank">
             <BsInstagram />
            </a>
            <a href="https://www.youtube.com/c/ONIMA" target="_blank">
              <BsYoutube />
            </a>
            <a href="#" target="_blank">
               <BsTwitter />
            </a>
            <a href="#" target="_blank">
              <BsLinkedin />
            </a>
          </div>
        </div>
      </section>
    </div>
    )
};
