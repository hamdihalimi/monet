import React ,{useState,useEffect} from 'react';
import './App.css';
import NavBar from "./NavBar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import  {Home} from "./components/pages/Home/Home";
import {Work} from "./components/pages/Work/Work";
import WorkScreen from './components/pages/Work/WorkScreen';
import {Services} from "./components/pages/Services/Services";
import {About} from "./components/pages/About/About";
import {Contact} from "./components/pages/Contact/Contact";




function App() {
 

  return (
    <>
  
      <Router className="page">
       <NavBar />
       <div className="pages">
        <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/work" component={Work} />
        <Route exact path="/workscreen/:id" component={WorkScreen} />
        <Route exact path="/services" component={Services} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />
        </Switch>
       </div>
     </Router>
 
    </>
  );
}

export default App;
